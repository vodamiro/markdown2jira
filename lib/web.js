import express from 'express'
import bodyParser from 'body-parser'

import parser from './parser/story_and_epic_parser'
import exporter from './exporter/exporter'

function run() {

    const port = process.argv[2] || 3000

    const app = express()

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(express.static('public'))

    app.get('/', (req, res, next) => {res.redirect('/index.html');})
    app.post('/generate/', (req, res, next) => {
        res.send(processRequest(req.body.projectKey, req.body.urlPrefix, Number(req.body.issueKeyOffset), req.body.content, JSON.parse(req.body.variants), JSON.parse(req.body.existingEpics)))
    })
    
    app.listen(port, () => console.log('Web running on port ' + port))
}

function processRequest(projectKey, url, issueKeyOffset, content, variants, existingEpics) {
    // Parse the content
    const epicsAndStories = parser(content)

    // Create JSON object from parsed epics and stories
    const object = exporter(epicsAndStories, projectKey, url, issueKeyOffset, variants, existingEpics)

    return JSON.stringify(object, null, "   ")
}

run()



