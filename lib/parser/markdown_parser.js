// Section
export const UNDEFINED = 0
export const NEW_LINE = 1
export const HEADER = 2
export const OTHER = 3 // content

// Convert String buffer to array of lines
function stringToLinesArray (content) {
    return content.toString().split(/(?:\r\n|\r|\n)/g)
}

// Returns true if given line is empty
function isBlankLine (line) {
    return (line.trim() == "")
}

// Returns true if line starts with # (Markdown header syntax)
function isHeader (line) {
    return line.startsWith("#")
}

// Returns level of header (1 for h1, 2 for h2, ..., 0 for non-header line)
function getHeaderLevel (line) {
    var level = 0 
    for (var ci = 0 ; ci < line.length ; ci++) {
        const char = line.charAt(ci)
        if (char == '#') {
            level = level + 1
        } else {
            break
        }
    }
    return level
}

// Returns content after sequence of # - header content
function getHeaderContent (line, level) {
    return line.substring(level + 1, line.length)
}

// Converts lines to array of semantic objects
function linesToObjects (lines) {
    var result = []
    for (var i in lines) {
        const line = lines[i]
        result.push(lineToObject(line))
    }
    return result
} 

// Converts string line to semantic object
function lineToObject (line) {
    var o = {
        type: UNDEFINED
    }
    if (isBlankLine(line)) {
        o.type = NEW_LINE
    } else if (isHeader(line)) {
        o.type = HEADER
        o.level = getHeaderLevel(line)
        o.content = getHeaderContent(line, o.level)
    } else {
        o.type = OTHER
        o.content = line
    }
    return o
}

export default function parse (content) {
    const lines = stringToLinesArray(content)
    return linesToObjects(lines)
}