import * as markdown from './markdown_parser'

function extractAttachments(content) {
    var attachments = []
    var re = /!\[([a-zA-Z0-9_ ,.\:+\-//\\%]*)\]\(([a-zA-Z0-9_ ,.\:+\-//\\%]*)\)/g
    var m
    
    do {
        m = re.exec(content)
        if (m) {
            attachments.push({alt: m[1], src: m[2]})
        }
    } while (m)
    const replacedText = content.replace(re, "")
    return {
        content: replacedText,
        attachments: attachments
    }
}

function extractTimeEstimation(content) {
    var timeEstimation = undefined
    var re = /\[[Oo]riginal[_]?[Ee]stimate="([A-z0-9\-]+)"\]/g
    var m

    do {
        m = re.exec(content)
        if (m) {
            timeEstimation = m[1]
        }
    } while (m)
    const replacedText = content.replace(re, "")
    return {
        content: replacedText,
        timeEstimation: timeEstimation
    }  
}

function processStory(stories, currentEpic, currentStory, currentContent, epicIndex) {
    var attachmentsResult = extractAttachments(currentContent.trim())
    var timeEstimationResult = extractTimeEstimation(attachmentsResult.content)
    stories.push({epic: currentEpic.trim(), name: currentStory.trim(), description: timeEstimationResult.content, epicIndex: epicIndex, attachments: attachmentsResult.attachments, timeEstimation: timeEstimationResult.timeEstimation})
}

// Converts semantic objects to epics and stories
function markdownToEpicsAndStories (objects) {

    var epics = []
    var stories = []

    var inStory = false

    var currentEpic = ""
    var currentStory = ""
    var currentContent = ""

    for (var i in objects) {
        const o = objects[i]

        if (o.type == markdown.HEADER) {
            if (o.level == 1) { // EPIC header

                if (inStory) { // Save current story
                    processStory(stories, currentEpic, currentStory, currentContent, epics.length)
                    currentStory = ""
                    currentContent = ""
                    inStory = false
                }
                
                currentEpic = o.content
                epics.push(currentEpic)
                
            } else if (o.level == 2) { // STORY header

                if (inStory) { // Save current story
                    processStory(stories, currentEpic, currentStory, currentContent, epics.length)
                    currentStory = ""
                    currentContent = ""
                    inStory = false
                }
        
                inStory = true
                currentStory = o.content
                
            } else { // Header in content
                if (inStory) { // Add header to story content
                    currentContent = currentContent + "\n" + o.content
                } else {
                    console.log("[L:" + i + "] Warning: Content out of story")
                }
            }
        } else if (o.type == markdown.NEW_LINE) {
            if (inStory) { // Add new line to story content
                currentContent = currentContent + "\n"
            }
        } else if (o.type == markdown.OTHER) {
            if (inStory) { // Add content to story content
                currentContent = currentContent + "\n" + o.content
            } else {
                console.log("[L:" + i + "] Warning: Content out of story")
            }
        }
    }

    // Add last one if any
    if (inStory) { // Save current story
        processStory(stories, currentEpic, currentStory, currentContent, epics.length)
        currentStory = ""
        currentContent = ""
        inStory = false
    }

    return {epics: epics, stories: stories}
}

export default function parse (content) {
    return markdownToEpicsAndStories(markdown.default(content))
}