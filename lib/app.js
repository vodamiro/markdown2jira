import reader from './reader/file_reader'
import parser from './parser/story_and_epic_parser'
import exporter from './exporter/exporter'
import writer from './exporter/file_writer'

function run() {
    // Check argument count
    if (process.argv.length < 4) {
        console.log("Error: Wrong arguments")
        console.log("")
        console.log("Usage: node index.js PROJECT_JIRA_KEY MARKDOWN_FILENAME [URL_TO_ATTACHMENTS] [ISSUE_KEY_OFFSET]")
        process.exit(1)
    }

    // Load parameters
    const projectKey = process.argv[2]
    const filename = process.argv[3]
    const url = process.argv[4] || ""
    const issueKeyOffset = Number(process.argv[5]) || 1

    // Read input file
    const content = reader(filename)

    // Parse the content
    const epicsAndStories = parser(content)

    const variants =  [
        {"prefix": "", "components": []}
    ]

    const existingEpics = {}

    // Create JSON object from parsed epics and stories
    const object = exporter(epicsAndStories, projectKey, url, issueKeyOffset, variants, existingEpics)

    // Save JSON object to JSON file
    writer(projectKey + ".json", object)
}

run()