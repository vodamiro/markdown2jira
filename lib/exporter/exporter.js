var output = {}
var offset = 0
var urlPrefix = ""
var epics = {}

function getIssueId () {
    return offset + output.projects[0].issues.length
}

function getIssueKey (projectKey) {
    return projectKey + "-" + getIssueId()
}

function exportStory (projectKey, story, variants) {
    for (var i in variants) {
        const variant = variants[i]
        exportStoryEx(projectKey, story, variant.prefix, variant.components)
    }
}

function addEpic(epicName, epicId) {
    epics[epicName] = epicId
}

function findEpicId(epicName) {
    return epics[epicName]
}

function createAttachments (story) {
    var attachments = []
    for (var i in story.attachments) {
        const attachment = story.attachments[i]
        attachments.push(
            {
                "name" : attachment.src,
                "uri" : urlPrefix + attachment.src,
                "description" : attachment.alt
            }
        )
    }
    return attachments
}

function exportStoryEx (projectKey, story, prefix, components) {
    output.projects[0].issues.push({
        issueType: "Story",
        key: getIssueKey(projectKey),
        externalId: getIssueId(),
        components: components,
        summary: prefix + story.name,
        description: story.description,
        epicLink: story.epic,
        originalEstimate: story.timeEstimation,
        customFieldValues: [
             {
                 "fieldName": "Epic Link",
                 "fieldType": "com.pyxis.greenhopper.jira:gh-epic-link",
                 "value": findEpicId(story.epic)
            }
        ],
        attachments: createAttachments(story.epic)
    })
}

function exportEpic (projectKey, epic) {
    const key = getIssueKey(projectKey)
    output.projects[0].issues.push({
        issueType: "Epic",
        key: key,
        externalId: getIssueId(),
        name: epic,
        epicName: epic,
        summary: epic,
        customFieldValues: [
            {
                "fieldName": "Epic Name",
                "fieldType": "com.pyxis.greenhopper.jira:gh-epic-label",
                "value": epic
            }
        ]
    })
    return key
}

export default function exp (epicsAndStories, projectKey, urlToAttachments, issueKeyOffset, variants, existingEpics) {
    urlPrefix = urlToAttachments
    offset = issueKeyOffset
    if (existingEpics) {
        epics = existingEpics
    }

    // Init result object
    output.projects = [{
        key: projectKey,
        issues: []
    }]

    // Create Non-existings Epic issues 
    for (var i in epicsAndStories.epics) {
        const epic = epicsAndStories.epics[i]
        if (epics[epic] == null || epics[epic] == undefined) {
            addEpic(epic, exportEpic(projectKey, epic)) // Exports epic to output and add returned ID of epic to epics map
        }
    }

    // Create Story issues
    for (var i in epicsAndStories.stories) {
        const story = epicsAndStories.stories[i]
        exportStory(projectKey, story, variants)
    }

    return output
}