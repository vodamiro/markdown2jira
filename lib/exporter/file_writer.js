import fs from 'fs'

export default function save (filename, content) {
    fs.writeFileSync(filename, JSON.stringify(content ,null ,"   "), {encoding: 'utf8'})
    console.log("Saved to file: " + filename)
}