markdown2jira
===

Node.js library that converts Markdown file to JIRA JSON file ready for import.

## Markdown Epics and Stories syntax

Source file must be in Markdown syntax. H1 headers are associated as epics. H2 headers are stories. All content under H2 headers will be part of user story as a description.

Input file must have at least one epic.

```Markdown
# First epic

## As a user I want this story in my JIRA project.

Some *description* of user story.

More text:
* aaa
* bbb
* ccc

Time estimation is 1 week 3 days 4 hours 20 minutes. [OriginalEstimate="P1W3DT4H20M"]

## As a user I want this second story in my JIRA project.

Another description.

# Second epic

## As a user I want this story in second epic.

A description.

!(Image description)[http://www.img.com/img1.jpg]

```

## Usage

### Console app

`npm start PROJECT_JIRA_KEY MARKDOWN_FILENAME [URL_TO_ATTACHMENTS] [ISSUE_KEY_OFFSET]`

`PROJECT_JIRA_KEY` - JIRA project key. For example "ABC"

`MARKDOWN_FILENAME` - Path to source Markdown file

`[URL_TO_ATTACHMENTS]` - Prefix for declared images (optional). Default: ""

`[ISSUE_KEY_OFFSET]` - ID of first Epic (optional). Default: 1

Generated JSON file can be used for import to JIRA.

### Web app

`node webapp.js [PORT]` - Starts web server. Can be accessed through web browser on `http://localhost:PORT/`, where

`PORT` - port number on which server listens on, defaultly port `3000`.

### Docker web app

There is a Dockerfile that creates Docker container running web application. Exposes port `3000`.

# Useful regular expressions:

If you want to convert exported Epic CSV lines to JSON map, use following Regex:
Search for: `^[A-z ]+,([A-z0-9\-]+),[0-9]+,([À-žA-z0-9\- \.]+),.*` and replace it with `"$2": "$1",` then you will 
get almost final JSON file (you need to replace first line with `{` and last `,` character with `}`. 